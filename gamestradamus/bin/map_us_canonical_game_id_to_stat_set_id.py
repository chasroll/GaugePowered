# -*- coding: utf-8 -*-
import sys
import string
import datetime
from geniusql import logic
from gamestradamus import cli, units, timeutils
from gamestradamus.taskqueue import tasks
from gamestradamus.bin.find_equivalent_games import find_duplicates, clean_name

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    """Run this once after deploying the StatSet change to ensure they are mapped.

    This will go through and look at the canonical_game_id set for all games and
    update them to be a part of the same stat set.
    """

    store = cli.storage_manager(conflicts='repair')
    sandbox = store.new_sandbox()


    games = sandbox.recall(units.Game)
    formatted_available = {
            False: " ",
            True: "Y",
        }
    for game in games:
        # if they already have the stat set id, skip them.
        if game.stat_set_id:
            continue

        duplicate_games = sandbox.recall(
                units.Game, lambda g: g.canonical_game_id == game.id
            )
        if (duplicate_games):
            stat_set = units.StatSet()
            sandbox.memorize(stat_set)
            for g in [game] + duplicate_games:
                g.stat_set_id = stat_set.id

    sandbox.flush_all()
    store.shutdown()
