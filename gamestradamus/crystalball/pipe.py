# -*- coding: utf-8 -*-
"""A web service interface to the crystal ball indexes that are used.
"""
from __future__ import absolute_import

import cherrypy
import os
import sys
import os.path

from gamestradamus.crystalball import search

#-------------------------------------------------------------------------------
def __handlers__(root):
    """
    Setup the necessary handlers
    """
    local_dir = os.path.join(os.getcwd(), os.path.dirname(__file__))
    indexes = search.Search()
    indexes_directory = os.path.join(local_dir, cherrypy.config.get("gamestradamus.crystalball.path"))

    count = 0
    print "Adding Indexes ..."
    sys.stdout.flush()
    for root_directory, dirs, files in os.walk(indexes_directory):
        for filename in files:
            print filename
            if filename.endswith(".crystalball"):
                app_id = filename.replace(".crystalball", "")
                app_id = app_id.replace("game_", "")
                app_id = int(app_id)
                file_path = os.path.join(root_directory, filename)
                indexes.add_index(app_id, file_path)
                count += 1
    print "Added %s indexes" % (count,)

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @cherrypy.tools.json_out()
    def index(self, steam_app_id, hours_played, count):
        steam_app_id = int(steam_app_id)
        hours_played = float(hours_played)
        count = int(count)
        return indexes.consumers_similar_to(steam_app_id, hours_played, count)
    root.index = index.__get__(root)


