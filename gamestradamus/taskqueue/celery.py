from __future__ import absolute_import

import os

from kombu import Queue
from celery import Celery, chain
import cherrypy
from cherrypy.lib import reprconf
from gamestradamus import cli


global_conf = cli.get_global_config()
celery = Celery('gamestradamus.taskqueue',
                broker=global_conf.get('celery.broker'),
                include=['gamestradamus.taskqueue.tasks'])

# We have three main task priorities, so we'll make a queue for each:
celery.conf.update(CELERY_DEFAULT_QUEUE='plebs')
celery.conf.update(CELERY_QUEUES=(
    # 1. New user signins which are the highest priority
    Queue('welcome', routing_key='tasks.welcome', durable=True),
    # 2. Premium user tasks
    Queue('premium', routing_key='tasks.premium.#', durable=True),
    # 3. freeloader tasks
    Queue('plebs', routing_key='tasks.plebs.#', durable=True),
    # 4. background tasks
    Queue('background', routing_key='tasks.background.#', durable=True),
))
celery.conf.update(CELERY_DEFAULT_EXCHANGE='tasks')
celery.conf.update(CELERY_DEFAULT_EXCHANGE_TYPE='topic')
celery.conf.update(CELERY_DEFAULT_ROUTING_KEY='tasks.plebs')
celery.conf.update(CELERY_SEND_EVENTS=True)
# Enables error emails.
celery.conf.update(CELERY_SEND_TASK_ERROR_EMAILS=True)

# Name and email addresses of recipients
celery.conf.update(ADMINS = (("Gauge Admins", global_conf['gauge.email.error_address'],),))
celery.conf.update(SERVER_EMAIL=global_conf['gauge.email.server_address'])
# Mailserver configuration
celery.conf.update(EMAIL_HOST=global_conf["gauge.smtp.host"])
celery.conf.update(EMAIL_PORT=global_conf["gauge.smtp.port"])
celery.conf.update(EMAIL_HOST_USER=global_conf["gauge.email.server_address"])
celery.conf.update(EMAIL_HOST_PASSWORD=global_conf["gauge.email.password"])
celery.conf.update(EMAIL_USE_TLS=global_conf["gauge.smtp.use_tls"])

# The only task that we can route by name is the welcome task
celery.conf.update(CELERY_ROUTES={
    'gamestradamus.taskqueue.tasks.welcome': {
        'queue': 'welcome',
        'routing_key': 'tasks.welcome',
    },
})

if __name__ == '__main__':
    celery.start()
