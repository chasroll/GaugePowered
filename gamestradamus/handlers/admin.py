"""Some administration based handlers that should only be accessible by us.
"""
import cream.tools
import cherrypy
import datetime

from plumbing import auth, email as plumbing_email

from gamestradamus import units

ALLOWED_IDS = [1, 2, 3] # Not real. ;)


#-------------------------------------------------------------------------------
# TODO: This is a candidate for plumbing
class RestrictedByUserId(object):
    """Decorator for enforcing that a valid session signin has occured.

    If the current session does not have a valid Account signed in,
    then the method issues a redirect to the index page.
    """

    def __init__(self, allowed_ids=None):
        """If allow_users is None, it allows all signed in users.

        If allow_users is not None, then the user id MUST be in the given list.
        """
        super(RestrictedByUserId, self).__init__()
        self._allowed_ids = allowed_ids

    def __call__(self, func):
        def wrapper(*args, **kwargs):
            # First and foremost, ensure they are signed in
            request = cherrypy.serving.request
            if not hasattr(request, 'user_account') or request.user_account is None:
                raise cherrypy.NotFound()

            # if _allowed_id is set, filter this to only those ids
            if self._allowed_ids is not None:
                if request.user_account.id not in self._allowed_ids:
                    raise cherrypy.NotFound()
            return func(*args, **kwargs)

        wrapper.__name__ = func.__name__
        wrapper.__doc__ = func.__doc__
        wrapper.__dict__.update(func.__dict__)
        return wrapper

#-------------------------------------------------------------------------------
class AdminHandler(object):

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @RestrictedByUserId(allowed_ids=ALLOWED_IDS)
    @cream.tools.JinjaTemplate('/gamestradamus/admin/index.html')
    def index(self, **kwargs):
        return {}

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @RestrictedByUserId(allowed_ids=ALLOWED_IDS)
    @cream.tools.JinjaTemplate('/gamestradamus/admin/duplicates.html')
    def duplicates(self, **kwargs):
        request = cherrypy.request
        duplicates = request.sandbox.recall(
                units.PossibleDuplicate,
                lambda dupe: (
                    dupe.resolved == False or dupe.resolved is None
                )
            )
        return {
            'duplicates': duplicates,
            'duplicates_len': len(duplicates),
        }

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @RestrictedByUserId(allowed_ids=ALLOWED_IDS)
    @cream.tools.JinjaTemplate('/gamestradamus/admin/duplicate.html')
    def duplicate(self, duplicate_id, **kwargs):
        request = cherrypy.request
        sandbox = request.sandbox
        duplicate = sandbox.unit(
                units.PossibleDuplicate,
                id=duplicate_id,
            )
        if not duplicate:
            raise cherrypy.NotFound()

        error = ''
        if request.method == "POST":
            false_positive = kwargs.get('false_positive', False) == 'on'
            stat_set_duplicate = kwargs.get('stat_set_duplicate', False) == 'on'
            game_1_canonical_duplicate = kwargs.get('game_1_canonical_duplicate', False) == 'on'
            game_2_canonical_duplicate = kwargs.get('game_2_canonical_duplicate', False) == 'on'
            game_1 = duplicate.game_1()
            game_2 = duplicate.game_2()


            if false_positive:
                # False positive is one option.
                duplicate.resolved = True
                raise cherrypy.HTTPRedirect('/gauge_admin/duplicates/')
            else:
                if game_1_canonical_duplicate and game_2_canonical_duplicate:
                    error = "You can't mark both as canonical duplicates?!"

                elif game_1_canonical_duplicate:
                    duplicate.resolved = True
                    target_game = game_2.get_canonical_game()
                    game_1.canonical_game_id = target_game.id

                elif game_2_canonical_duplicate:
                    duplicate.resolved = True
                    target_game = game_1.get_canonical_game()
                    game_2.canonical_game_id = target_game.id

                if stat_set_duplicate:
                    duplicate.resolved = True
                    # If they BOTH have a stat_set, then we must merge them.
                    if game_1.stat_set_id is not None and game_2.stat_set_id is not None:
                        # Do nothing, the'yre already part of the same stat set
                        if game_1.stat_set_id == game_2.stat_set_id:
                            pass
                        else:
                            # Move all the games in the stat_set of game_2 into
                            # the same stat_set as game_1
                            for game in sandbox.recall(
                                    units.Game,
                                    lambda g: g.stat_set_id == game_2.stat_set_id
                                    ):
                                game.stat_set_id = game_1.stat_set_id

                    # if only one has a stat_set, then it's easy.
                    elif game_1.stat_set_id is not None or game_2.stat_set_id is not None:
                        if game_1.stat_set_id:
                            game_2.stat_set_id = game_1.stat_set_id
                        elif game_2.stat_set_id:
                            game_1.stat_set_id = game_2.stat_set_id

                    # in this case we need to add them BOTH to the same
                    # stat_set
                    else:
                        stat_set = units.StatSet()
                        sandbox.memorize(stat_set)
                        game_1.stat_set_id = stat_set.id
                        game_2.stat_set_id = stat_set.id

                # If no error was specified, redirect.
                if not error:
                    raise cherrypy.HTTPRedirect('/gauge_admin/duplicates/')
        return {
            'error': error,
            'duplicate': duplicate,
            'now': datetime.datetime.now()
        }
