//------------------------------------------------------------------------------
// A program which will create the index for a particular game.
//------------------------------------------------------------------------------
#include <gamestradamus/steam/search.hpp>
#include <gamestradamus/steam/game.hpp>
#include <gamestradamus/steam/account.hpp>
#include <gamestradamus/steam/accountgame.hpp>

#include <boost/cstdint.hpp>
#include <boost/foreach.hpp>
#include <boost/program_options.hpp>
#include <boost/timer/timer.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include <fstream>
#include <iostream>
#include <strstream>
#include <string>

#include <pqxx/pqxx>

using namespace gamestradamus::steam;
using namespace std;
namespace po = boost::program_options;

//------------------------------------------------------------------------------
// TODO: this connection information should be passed in from the command line
//       or a config file.
std::string db_connection_string =
    "dbname=xxx user=xxx password=xxx host=";

//------------------------------------------------------------------------------
// Simple helper to get the required variables from the command line
template <class T>
T required_param(const po::variables_map &vm, const std::string name) {
    if (vm.count(name)) {
       return vm[name].as<T>();
    } else {
        cerr << name << " was not set.\n";
        std::exit(1);
    }
}

//------------------------------------------------------------------------------
int main(int argc, char **argv) {

    //--------------------------------------------------------------------------
    // Parse the program options
    po::options_description desc("Allowed options");
    desc.add_options()
        ("bucket_size", po::value<int>(), "Search bucket size")
        ("app_id", po::value<std::string>(), "Build the index for this Game")
        ("filename", po::value<std::string>(), "Output file name for index")
        ("hostname", po::value<std::string>(), "hostname for the database")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    int bucket_size = required_param<int>(vm, "bucket_size");
    std::string app_id = required_param<std::string>(vm, "app_id");
    std::string filename = required_param<std::string>(vm, "filename");
    std::string hostname = required_param<std::string>(vm, "hostname");

    std::vector<AccountGame> records;
    boost::timer::cpu_timer sql_time, build_time, archive_time;

    //--------------------------------------------------------------------------
    // GET ALL of the games and their playtimes and build the indexes.
    pqxx::connection connection(db_connection_string + hostname);

    {
        pqxx::work W(connection, "Get the hours played for a given app_id");

        // Get the game id for this particular app
        string get_game_sql = "SELECT \"Game\".id AS id FROM \"Game\" WHERE \"Game\".app_id = " + app_id;
        pqxx::result game = W.exec(get_game_sql);
        std::string id;
        game.at(0)["id"].to(id);

        // Get apps marked as equivalent
        std::ostringstream o;
        o << "(" << id;

        string get_equivalent_games = "SELECT \"Game\".id AS id FROM \"Game\" WHERE \"Game\".canonical_game_id = " + id;
        pqxx::result equivalent_games = W.exec(get_equivalent_games);
        for (pqxx::result::const_iterator game = equivalent_games.begin(); game != equivalent_games.end(); ++game) {
            std::string game_id;
            (*game)["id"].to(game_id);
            o << ", " << game_id;
        }
        o << ")";

        std::string get_hours_played = "" \
                "SELECT \"UserAccountGame\".steam_hours_played AS hours_played, " \
                " \"UserAccount\".openid AS openid " \
                " FROM \"UserAccount\" " \
                " INNER JOIN \"UserAccountGame\" " \
                " ON \"UserAccountGame\".user_account_id = \"UserAccount\".id " \
                " LEFT JOIN \"UserAccountAttributes\" " \
                " ON \"UserAccountAttributes\".user_account_id = \"UserAccount\".id " \
                " WHERE \"UserAccountGame\".game_id IN " + o.str() + \
                " AND \"UserAccountGame\".steam_hours_played > 0.0 " \
                " AND \"UserAccountAttributes\".private_profile_since IS NULL " \
                " ORDER BY \"UserAccountGame\".steam_hours_played ASC, \"UserAccount\".openid ASC";

        sql_time.start();
        pqxx::result game_players = W.exec(get_hours_played);
        sql_time.stop();

        build_time.start();
        Game::id_t game_id;

        std::istringstream i(app_id);
        i >> game_id;

        std::string base = "http://steamcommunity.com/openid/id/";
        for (pqxx::result::const_iterator player = game_players.begin(); player != game_players.end(); ++player) {
            Account::id_t account_id;
            std::string openid;
            AccountGame::hours_t hours_played;
            (*player)["openid"].to(openid);
            (*player)["hours_played"].to(hours_played);
            if (openid == "") {
                continue;
            }
            std::string steam_id_64_str = openid.substr(base.length(), std::string::npos);
            std::istringstream id(steam_id_64_str);
            id >> account_id;

            records.push_back(AccountGame(account_id, game_id, hours_played));
        }
    }
    ConsumerTraitBucketSearch ctbs(bucket_size, records);
    build_time.stop();

    // Write it out to the index
    archive_time.start();
    std::ofstream ofs(filename);
    {
        boost::archive::binary_oarchive oa(ofs);
        oa << ctbs;
    }
    archive_time.stop();

    //cout << "SQL Time: " << sql_time.format();
    //cout << "Build Search Tree Time: " << build_time.format();
    //cout << "Archive time: " << archive_time.format();
    //cout << "Done!" << endl;
}
