//------------------------------------------------------------------------------
// Define a search test program
//------------------------------------------------------------------------------
#include <gamestradamus/steam/search.hpp>
#include <gamestradamus/steam/game.hpp>
#include <gamestradamus/steam/account.hpp>
#include <gamestradamus/steam/accountgame.hpp>

#include <boost/cstdint.hpp>
#include <boost/foreach.hpp>
#include <boost/program_options.hpp>
#include <boost/timer/timer.hpp>

#include <iostream>
#include <string>
#include <sstream>

#include <pqxx/pqxx>

using namespace gamestradamus::steam;
using namespace std;
namespace po = boost::program_options;

//------------------------------------------------------------------------------
// Some hard coded helper ids for us to test things out.
Account::id_t lakin_id = 0;
Account::id_t ltnooy_id = 0;
Game::id_t team_fortress_2_id = 0;

std::string lakin_id_str = "0";
std::string ltnooy_id_str = "0";
std::string team_fortress_2_id_str = "0";

std::string db_connection_string =
    "dbname=xxxx user=xxxx password=xxxx host=localhost";

//------------------------------------------------------------------------------
std::string get_game_name(pqxx::connection &conn, int game_id) {
    pqxx::work transaction(conn, "Get Game ID");
    stringstream ss;


    ss << "SELECT name, store_url "
        << " FROM \"Game\" "
        << " WHERE id = " << game_id;
    string query = ss.str();

    pqxx::result games = transaction.exec(query);
    for (pqxx::result::const_iterator game = games.begin(); game != games.end(); ++game) {
        std::string name;
        std::string store_url;
        (*game)["name"].to(name);
        (*game)["store_url"].to(store_url);
        return name + " \t\t" + store_url;
    }
    return "";
}

//------------------------------------------------------------------------------
std::string get_vanity_url(pqxx::connection &conn, int account_id) {
    pqxx::work transaction(conn, "Get Game ID");
    stringstream ss;


    ss << "SELECT vanity_url "
        << " FROM \"Account\" "
        << " WHERE id = " << account_id;
    string query = ss.str();

    pqxx::result games = transaction.exec(query);
    for (pqxx::result::const_iterator game = games.begin(); game != games.end(); ++game) {
        std::string vanity_url;
        (*game)["vanity_url"].to(vanity_url);
        return vanity_url;
    }
    return "";
}

//------------------------------------------------------------------------------
int main(int argc, char **argv) {

    //--------------------------------------------------------------------------
    // Parse the program options
    po::options_description desc("Allowed options");
    desc.add_options()
        ("target_bin_size", po::value<int>(), "Set Target Bin Size")
        ("target_account_id", po::value<std::string>(), "Set Target Account ID")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    uint_fast32_t target_bin_size = 0;
    if (vm.count("target_bin_size")) {
       target_bin_size = vm["target_bin_size"].as<int>();
    } else {
        cerr << "Target Bin Size was not set.\n";
        return 1;
    }

    std::string target_account_id;
    if (vm.count("target_account_id")) {
       target_account_id = vm["target_account_id"].as<string>();
    } else {
        cerr << "Target account id was not set.\n";
        return 1;
    }
    AllGameSearch ags(target_bin_size);
    std::vector<AccountGame> current_game;
    boost::timer::cpu_timer sql_time, build_time, search_time;
    Game::id_t current_game_id = -1;

    //--------------------------------------------------------------------------
    // GET ALL of the games and their playtimes and build the indexes.
    pqxx::connection connection(db_connection_string);

    {
        pqxx::work transaction(connection, "Something something something");

        string query = "SELECT game_id, account_id, hours_played " \
                " FROM \"AccountGame\" " \
                " WHERE hours_played > 0.0 " \
                " ORDER BY game_id ASC, hours_played ASC, account_id ASC";

        sql_time.start();
        pqxx::result game_players = transaction.exec(query);
        sql_time.stop();
        for (pqxx::result::const_iterator player = game_players.begin(); player != game_players.end(); ++player) {
            Game::id_t game_id;
            Account::id_t account_id;
            AccountGame::hours_t hours_played;
            (*player)["game_id"].to(game_id);
            (*player)["account_id"].to(account_id);
            (*player)["hours_played"].to(hours_played);

            // Deal with the switch of games.
            if (current_game_id != game_id) {
                build_time.resume();
                ags.add_game(current_game_id, current_game);
                build_time.stop();
                current_game_id = game_id;
                current_game.clear();
            }
            current_game.push_back(AccountGame(account_id, game_id, hours_played));
        }
    }

    //--------------------------------------------------------------------------
    // Get all of a single users games/playtimes and search the index.
    std::vector<AccountGame> user_games;
    std::map<Game::id_t, int> search_user_game_count;
    {
        pqxx::work transaction(connection, "Querying");

        string query = "SELECT game_id, account_id, hours_played " \
                " FROM \"AccountGame\" " \
                " WHERE hours_played > 0.0 AND \"account_id\" = " + target_account_id + \
                " ORDER BY hours_played DESC";

        pqxx::result user_game_results = transaction.exec(query);
        for (pqxx::result::const_iterator game_i = user_game_results.begin(); game_i != user_game_results.end(); ++game_i) {
            Game::id_t game_id;
            Account::id_t account_id;
            AccountGame::hours_t hours_played;
            (*game_i)["game_id"].to(game_id);
            (*game_i)["account_id"].to(account_id);
            (*game_i)["hours_played"].to(hours_played);

            user_games.push_back(AccountGame(account_id, game_id, hours_played));
            search_user_game_count[game_id] = 1;
        }
    }

    //--------------------------------------------------------------------------
    // Time the speed of doing 1000 queries
    unsigned int num_requests = 1;
    search_time.start();
    for (unsigned int i = 0; i < num_requests; ++i) {
        boost::heap::fibonacci_heap<Match> results = ags.search(user_games);

        typedef map<int, int> games_owned_t;
        games_owned_t games_owned_map;
        typedef map<int, double> games_owned_factor_t;
        games_owned_factor_t games_owned_factor_map;
        typedef map<int, double> games_owned_playtime_t;
        games_owned_playtime_t games_owned_playtime_map;
        typedef map<int, double> games_owned_average_playtime_t;
        games_owned_average_playtime_t games_owned_average_playtime_map;

        cout << results.size() << " search results" << endl;
        int count = 0;
        while(results.size() > 0 && count < 200) {
            Match result = results.top();
            results.pop();
            count++;
            int account_id = result.account_id;
            double factor = result.match_value;
            stringstream ss;
            ss << account_id;
            string account_id_str = ss.str();

            cout << get_vanity_url(connection, account_id) << " -> " << factor << " factor" << endl;
            //--------------------------------------------------------------------------
            // Get all of a single users games/playtimes and search the index.
            {
                pqxx::work transaction(connection, "Querying");

                string match_games = "SELECT game_id, account_id, hours_played " \
                        " FROM \"AccountGame\" " \
                        " WHERE hours_played > 0.0 AND \"account_id\" = " + account_id_str + \
                        " ORDER BY hours_played DESC";

                pqxx::result match_game_results = transaction.exec(match_games);
                for (pqxx::result::const_iterator match_game_i = match_game_results.begin(); match_game_i != match_game_results.end(); ++match_game_i) {
                    Game::id_t game_id;
                    AccountGame::hours_t hours_played;
                    (*match_game_i)["game_id"].to(game_id);
                    (*match_game_i)["hours_played"].to(hours_played);

                    games_owned_map[game_id]++;
                    games_owned_factor_map[game_id] += factor;
                    games_owned_playtime_map[game_id] += hours_played;
                }
            }


        }

        cout.precision(2);

        // Now calculate the top matches.
        boost::heap::fibonacci_heap<boost::tuple<int, int> > games_owned;
        BOOST_FOREACH(games_owned_t::value_type &game, games_owned_map) {
            games_owned_average_playtime_map[game.first] = (double)games_owned_playtime_map[game.first] / (double)game.second;
            games_owned.push(boost::make_tuple(game.second, game.first));
        }
        int game_count = 0;
        while(games_owned.size() > 0 && game_count < 150) {
            boost::tuple<int, int> result = games_owned.top();
            games_owned.pop();
            int game_id = result.get<1>();
            int owned_count = result.get<0>();
            if (owned_count < 25) {continue;}
            int already_owned = search_user_game_count[game_id];
            if (already_owned == 1) { continue; }
            cout << "Owned by: [" << owned_count << "]\t"
                << get_game_name(connection, game_id)
                << std::endl;
            game_count++;
        }
        boost::heap::fibonacci_heap<boost::tuple<double, int> > games_owned_factor;
        BOOST_FOREACH(games_owned_factor_t::value_type &game, games_owned_factor_map) {
            games_owned_factor.push(boost::make_tuple(game.second, game.first));
        }
        game_count = 0;
        while(games_owned_factor.size() > 0 && game_count < 150) {
            boost::tuple<double, int> result = games_owned_factor.top();
            games_owned_factor.pop();
            int game_id = result.get<1>();
            double factor = result.get<0>();
            int count = games_owned_map[game_id];
            if (count < 25) {continue;}
            int already_owned = search_user_game_count[game_id];
            if (already_owned == 1) { continue; }
            cout << "Factor: [" << fixed << factor << "]\t"
                << get_game_name(connection, game_id)
                << std::endl;
            game_count++;
        }
        boost::heap::fibonacci_heap<boost::tuple<double, int> > games_owned_playtime;
        BOOST_FOREACH(games_owned_playtime_t::value_type &game, games_owned_playtime_map) {
            games_owned_playtime.push(boost::make_tuple(game.second, game.first));
        }
        game_count = 0;
        while(games_owned_playtime.size() > 0 && game_count < 150) {
            boost::tuple<double, int> result = games_owned_playtime.top();
            games_owned_playtime.pop();
            int game_id = result.get<1>();
            double playtime = result.get<0>();
            int count = games_owned_map[game_id];
            if (count < 25) {continue;}
            int already_owned = search_user_game_count[game_id];
            if (already_owned == 1) { continue; }
            cout << "Total Playtime: [" << fixed << playtime << "]\t"
                << get_game_name(connection, game_id)
                << std::endl;
            game_count++;
        }
        boost::heap::fibonacci_heap<boost::tuple<double, int> > games_average_playtime;
        BOOST_FOREACH(games_owned_average_playtime_t::value_type &game, games_owned_average_playtime_map) {
            games_average_playtime.push(boost::make_tuple(game.second, game.first));
        }
        game_count = 0;
        while(games_average_playtime.size() > 0 && game_count < 150) {
            boost::tuple<double, int> result = games_average_playtime.top();
            games_average_playtime.pop();
            int game_id = result.get<1>();
            double factor = result.get<0>();
            int count = games_owned_map[game_id];
            if (count < 25) {continue;}
            int already_owned = search_user_game_count[game_id];
            if (already_owned == 1) { continue; }
            cout << "Avg Playtime: [" << fixed << factor << "|" << count << "]\t"
                << get_game_name(connection, game_id)
                << std::endl;
            game_count++;
        }
    }
    search_time.stop();

    cout << "SQL Time: " << sql_time.format() << endl;
    cout << "Build Search Tree Time: " << build_time.format() << endl;
    cout << num_requests << " in time: " << search_time.format() << endl;

    cout << "Done!" << endl;
}
