//------------------------------------------------------------------------------
// Define a search engine.
//------------------------------------------------------------------------------
#ifndef GAMESTRADAMUS_STEAM_GAME_HPP
#define GAMESTRADAMUS_STEAM_GAME_HPP

#include <boost/cstdint.hpp> 
#include <boost/tuple/tuple.hpp> 

using namespace boost;

namespace gamestradamus {
namespace steam {


class Game {
public:
    typedef uint_fast32_t id_t;

    //--------------------------------------------------------------------------
    // Faux constructor.
    Game() { }



private:



};// end class Game

}//end namespace steam
}//end namespace gamestradamus

#endif//GAMESTRADAMUS_STEAM_GAME_HPP

