//------------------------------------------------------------------------------
// Define a python search module.
//------------------------------------------------------------------------------

#include <gamestradamus/steam/search.hpp>

#include <boost/python.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
using namespace boost::python;

#include <string>

namespace gamestradamus {
namespace python {
//------------------------------------------------------------------------------
class Search {
public:
    //--------------------------------------------------------------------------
    // Empty constructor
    Search() {}

    //--------------------------------------------------------------------------
    // Empty destructor
    virtual ~Search() {}

    //--------------------------------------------------------------------------
    void add_index(std::string filename) {
        // TODO: make this an auto_ptr
        ConsumerTraitBucketSearch *ctbs = new ConsumerTraitBucketSearch();
        std::ifstream ifs(filename);
        {
            boost::archive::text_iarchive ia(ofs);
            ia << ctbs;
        }
        my_search.add_game(ctbs);
    }

private:
    // Store a reference to the all game search.
    AllGameSearch my_search;

}; // End class Search

}//end namespace python
}//end namespace gamestradamus

BOOST_PYTHON_MODULE(search)
{
    class_<Search>("Search")
        .def("add_index", &Search::add_index)
    ;
}
