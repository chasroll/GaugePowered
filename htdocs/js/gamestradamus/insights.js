define([
    "atemi/io/jsonrpc",
    "atemi/util/format",

    "bootstrap/Tooltip",
    "bootstrap/Modal",

    "dojo/_base/array",
    "dojo/_base/event",
    "dojo/_base/fx",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/mouse",
    "dojo/on",
    "dojo/query",

    "gamestradamus/analytics/internal",
    "gamestradamus/region",

    "dojo/NodeList-traverse"
], function(
    jsonrpc,
    format,

    Tooltip,
    Modal,

    array,
    event,
    fx,
    dom,
    domClass,
    domConstruct,
    mouse,
    on,
    query,

    analytics,
    region
) {
    //--------------------------------------------------------------------------
    var setup_premium_nagging = function() {
        var modal = dom.byId('premium-modal');
        on(query('a.premium-support'), 'click', function(evt) {
            event.stop(evt);
            var anchor = evt.currentTarget;

            query(modal).modal('show');  
        }); 
    };
    setup_premium_nagging();

    //--------------------------------------------------------------------------
    var spectrum_into_filter_parameter = function(spectrum) {
        /**
         * return the appropriate library filter parameter for a given spectrum.
        **/
        if (spectrum == 'Library/Hours') {
            return 'hours';
        } else if (spectrum == 'Library/Cost') {
            return 'cost';
        } else if (spectrum == 'Library/Finished') {
            return 'finished';
        } else if (spectrum == 'Library/Enjoyment') {
            return 'enjoyment';
        } else if (spectrum == 'Steam/Genre') {
            return 'genre';
        } else {
            return 'tag';
        }
    };

    //--------------------------------------------------------------------------
    var render_drilldown_chart = function(container, data, metric) {
        /**
         * Render a composite donut chart where each consecutive data set is a
         * breakdown of the previous sets elements.
         * 
         * container: The container node for the chart.
         * data: An array of data values.
         *
         * Note that all child nodes in the container element will be removed
         * when rendering.
         */

        if (!container) {
            return;
        }
        domConstruct.empty(container);

        var svg = d3.select(container).append('svg')
            .attr('width', 360)
            .attr('height', 380)
            .append('g')
            .attr('transform', 'translate(180, 190)');

        var precision = 1;
        if (metric == 'hp') {
            svg.append('text')
                .attr('dy', '.2em')
                .attr('text-anchor', 'middle')
                .attr('class', 'total')
                .text(function() {
                    return region['format-hours'](data.value).replace('&nbsp;', ' ');
                });
            svg.append('text')
                .attr('dy', '1.3em')
                .attr('text-anchor', 'middle')
                .attr('class', 'total-label')
                .text('hours');

        } else if (metric == 'dp') {
            precision = 2;
            svg.append('text')
                .attr('dy', '.35em')
                .attr('text-anchor', 'middle')
                .attr('class', 'total')
                .text(function() {
                    if (region['symbol-svg-position'] == 'before') {
                        return region['symbol-svg'] + region['format-dollars'](data.value).replace('&nbsp;', ' ');
                    } else {
                        return region['format-dollars'](data.value).replace('&nbsp;', ' ') + region['symbol-svg'];
                    }
                });
        }
        var sum = data.value;

        var render_slice = function(g, set, angle, radius, colors, depth) {
            var bounds = d3.extent(set, function(d) { return d.value; });
            var color = d3.scale.linear()
                .domain([bounds[1], bounds[0]])
                .range([colors[0], colors[1]]);
            var arc = d3.svg.arc()
                .innerRadius(radius.inner)
                .outerRadius(radius.outer);
            var pie = d3.layout.pie()
                .sort(null)
                .startAngle(angle.start)
                .endAngle(angle.end)
                .value(function(d) { return d.value; });

            var g = d3.select(g)
                .datum(set)
                .selectAll('.arc')
                .data(pie)
                .enter()
                .append('g')
                .attr('class', function(d) {
                    var cls = d.data.active ? 'active' : '';
                    if ((d.value / sum * 100) < 5.5) {
                        return 'arc insignificant ' + cls;
                    } else {
                        return 'arc ' + cls;
                    }
                })
                .attr('title', 'Click to pin this value')
                .attr('data-label', function(d) { return d.data.label; })
                .attr('data-percentage', function(d) {
                    return ((d.value / sum) * 100).toFixed(precision);
                })
                .attr('data-value', function(d) {
                    return d.value.toFixed(precision);
                })
                .attr('data-count', function(d) {
                    return d.data.count.toFixed(0);
                })
                .attr('data-source', function(d) {
                    var source = d.data.source;
                    var encoded, piece; 
                    for (var i = 0; i < source.length; ++i) {
                        piece = source[i].id + '//' + source[i].name + '//' + source[i].value.toFixed(precision);
                        if (!encoded) {
                            encoded = piece;
                        } else {
                            encoded += '||' + piece;
                        } 
                    }
                    return encoded;
                })
                .attr('data-filter', function(d) {
                    return d.data.filter;
                });

            var title = g.append('title')
                .text('Click to pin this value');

            var path = g.append('path')
                .attr('d', arc)
                .attr('fill', function(d) { return color(d.value); });

            var text = g.append('text')
                .attr('transform', function(d) { return 'translate(' + arc.centroid(d) + ')'; })
                .attr('dy', '.35em')
                .attr('text-anchor', 'middle')
                .text(function(d) {
                    var p = (d.value / sum) * 100;
                    if (p > (6 - depth * 2)) {
                        return p.toFixed(0);
                    }
                })
                .append('tspan')
                .text(function(d) {
                    var p = (d.value / sum) * 100;
                    if (p > (6 - depth * 2)) {
                        return '%';
                    }
                });

            // Animate the arcs from empty to full.
            var arc_tween = function(a) {
                var i = d3.interpolate(this._current, a);
                this._current = i(0);
                return function(t) {
                    return arc(i(t));
                };
            };

            pie.startAngle(0).endAngle(0);
            path.data(pie)
                .attr('d', arc)
                .each(function(d) { this._current = d; });

            pie.startAngle(angle.start).endAngle(angle.end);
            path.data(pie)
                .transition()
                .duration(750)
                .ease('quad-in-out')
                .attrTween('d', arc_tween); 

            // Render further data breakdowns, recursively.
            arc_set = pie(set);
            array.forEach(arc_set, function(arc, i) {
                if (!arc.data.breakdown.length) { 
                    return;
                }
                render_slice(
                    g[0][i],
                    arc.data.breakdown,
                    {start: arc.startAngle, end: arc.endAngle},
                    {inner: radius.inner + 38, outer: radius.outer + 38},
                    arc.data.colors,
                    depth + 1 
                );
            });
        };
        render_slice(
            svg[0][0],
            data.breakdown,
            {start: 0, end: 6.2831852},
            {inner: 40, outer: 78},
            data.colors,
            0 
        );
    };

    //--------------------------------------------------------------------------
    var add_drilldown_interaction = function(metric, library_path) {
        /**
         * Add interaction behaviour to the drilldown chart.
        **/

        var update_display = function(duration, metric) {

            if (!duration) {
                duration = 750;
            }

            var active = query('g.arc.quick-active', 'drilldown').pop();
            if (!active) {
                active = query('g.arc.active', 'drilldown').pop();
            }
            if (!active) {
                return;
            }
            
            if (metric == 'hp') {
                query('>.units.post', 'arc-value')[0].innerHTML = 'hrs.';
                query('>.units.pre', 'arc-value')[0].innerHTML = '';
            } else if (metric == 'dp') {
                if (region['symbol-position'] == 'before') {
                    query('>.units.pre', 'arc-value')[0].innerHTML = region['symbol'];
                    query('>.units.post', 'arc-value')[0].innerHTML = '';
                } else {
                    query('>.units.post', 'arc-value')[0].innerHTML = region['symbol'];
                    query('>.units.pre', 'arc-value')[0].innerHTML = '';
                }
            }

            // Animate the change in value.
            var value_node = query('strong', 'arc-value')[0];
            var value = {
                initial: parseFloat(value_node.getAttribute('data-value')),
                target: parseFloat(active.getAttribute('data-value'))
            };
            d3.select('strong', 'arc-value')
                .transition()
                .duration(duration)
                .ease('quad-in-out')
                .tween("value", function() {
                    var i = d3.interpolate(value.initial, value.target);
                    return function(t) {
                        if (metric == 'hp') {
                            value_node.innerHTML = region['format-hours'](i(t));
                            value_node.setAttribute('data-value', i(t).toFixed(1));
                        } else if (metric == 'dp') {
                            value_node.innerHTML = region['format-dollars'](i(t));
                            value_node.setAttribute('data-value', i(t).toFixed(2));
                        }
                    };
                });

            var labels = active.getAttribute('data-label').split(' ∩ ');
            var label = 'on:';
            for (var i = 0; i < labels.length; ++i) {
                label += '<em>' + labels[i] + '</em>';
                if (i < labels.length - 1) {
                    label += '∩';
                }
            }
            dom.byId('arc-label').innerHTML = label;

            // Update the details about which games that are included in the arc value.
            var title_count_node = query('>.by-details >.title >span', 'aggregates')[0];
            if (active.getAttribute('data-count') == '1') {
                title_count_node.innerHTML = '1 Game'; 
            } else {
                title_count_node.innerHTML = active.getAttribute('data-count') + ' Games';
            }

            var anchor_list = query('>.by-details >a', 'aggregates');
            for (var i = 0; i < 2; ++i) {
                anchor_list[i].innerHTML = '';
                anchor_list[i].href = '';
            }

            var source_list = active.getAttribute('data-source').split('||');
            for (var i = 0; i < source_list.length; ++i) {
                var source = source_list[i].split('//');
                if (metric == 'hp') {
                    anchor_list[i].innerHTML = '(' + region['format-hours'](source[2]) + ' hrs) - ' + source[1];  
                } else if (metric == 'dp') {
                    if (region['symbol-position'] == 'before') {
                        anchor_list[i].innerHTML = '(' + region['symbol'] + region['format-dollars'](source[2]) + ') - ' + source[1];
                    } else {
                        anchor_list[i].innerHTML = '(' + region['format-dollars'](source[2]) + region['symbol'] + ') - ' + source[1];
                    }
                }
                anchor_list[i].href = '/game/' + source[0] + '/';
            }
            anchor_list[2].href = library_path + '#' + active.getAttribute('data-filter');
        };
        update_display(750, metric);

        // Hovering and clicking on the arcs updates the global statistics.
        query('g.arc', 'drilldown').on('click', function(evt) {
            event.stop(evt);

            if (metric == 'hp') {
                analytics.trackEvent('Time Insights', 'Drilldown', 'Slice');
            } else {
                analytics.trackEvent('Money Insights', 'Drilldown', 'Slice');
            }

            // Unset any previously active arcs.
            query('g.arc', 'drilldown').forEach(function(arc) {
                arc.setAttribute(
                    'class',
                    arc.getAttribute('class').replace(' active', '')
                );
            });

            // Make the clicked arc active as well as any of its parents.
            evt.currentTarget.setAttribute(
                'class',
                evt.currentTarget.getAttribute('class')  + ' active'
            );
            query(evt.currentTarget).parents('g').forEach(function(arc) {
                var cls = arc.getAttribute('class');
                if (cls && cls.indexOf('arc') > -1) {
                    arc.setAttribute('class', cls + ' active');
                }
            });

            update_display(125, metric);
        });

        query('g.arc', 'drilldown').on(mouse.enter, function(evt) {
            evt.currentTarget.setAttribute(
                'class',
                evt.currentTarget.getAttribute('class')  + ' quick-active'
            );

            update_display(125, metric);
        });
        query('g.arc', 'drilldown').on(mouse.leave, function(evt) {
            evt.currentTarget.setAttribute(
                'class',
                evt.currentTarget.getAttribute('class').replace(' quick-active', '')
            );

            update_display(125, metric);
        });
    };

    //--------------------------------------------------------------------------
    var render_composite_chart = function(
        container,
        data,
        metric,
        spectrum,
        colors,
        library_path
    ) {
        /**
         * Render a donut chart. 
         *
         * container: The container node for the chart.
         * data: An array of data values.
         * metric: The value from the game record to show (hours, dollars, etc)
         * spectrum: The spectrum across which the data in this chart was grouped.
         * colors: An array of primary colors.
         * library_path: The url to the library page to get more details on.
         *
         * Note that all child nodes in the container element will be removed
         * when rendering.
        **/

        if (!container) {
            return;
        }
        domConstruct.empty(container);

        var svg = d3.select(container).append('svg')
            .attr('width', 162)
            .attr('height', 162)
            .append('g')
            .attr('transform', 'translate(81, 81)');

        // Render the composite donut chart.
        var bounds = d3.extent(data, function(d) { return d.value; });
        var color = d3.scale.linear()
            .domain([bounds[1], bounds[0]])
            .range([colors[0], colors[1]]);
        var arc = d3.svg.arc()
            .innerRadius(40)
            .outerRadius(78);
        var pie = d3.layout.pie()
            .sort(null)
            .value(function(d) { return d.value; });
        var sum = d3.sum(data, function(d) { return d.value; });

        var g = svg.datum(data)
            .selectAll('.arc')
            .data(pie)
            .enter()
            .append('g')
            .attr('class', 'arc');
        
        var path = g.append('path')
            .attr('d', arc)
            .attr('fill', function(d) { return color(d.value); });

        var text = g.append('text')
            .attr('transform', function(d) { return 'translate(' + arc.centroid(d) + ')'; })
            .attr('dy', '.35em')
            .attr('text-anchor', 'middle')
            .text(function(d) {
                if ((d.value / sum * 100) > 6) {
                    return (d.value / sum * 100).toFixed(0);
                }
            })
            .append('tspan')
            .text(function(d) {
                if ((d.value / sum * 100) > 6) {
                    return '%';
                }
            });

        // Animate the arcs from empty to full.
        var arc_tween = function(a) {
            var i = d3.interpolate(this._current, a);
            this._current = i(0);
            return function(t) {
                return arc(i(t));
            };
        };

        pie.startAngle(0).endAngle(0);
        path.data(pie)
            .attr('d', arc)
            .each(function(d) { this._current = d; });

        pie.startAngle(0).endAngle(6.2831852);
        path.data(pie)
            .transition()
            .duration(750)
            .ease('quad-in-out')
            .attrTween('d', arc_tween);

        // Render the individual breakdown details.
        var parents = query('.arc', container);
        array.forEach(data, function(stat, j) {
            if (stat.value <= 0) {
                return;
            }

            var node = domConstruct.create('div');
            var template = 
                '<div class="chart">' +
                '   <div class="title"><em></em><span>%</span></div>' +
                '</div>' +
                '<h3></h3>'; 
            if (metric == 'hp') {
                template += '<p><strong class="value"></strong>hrs</p>'; 
            } else if (metric == 'dp') {
                template += '<p><strong class="value"></strong></p>';
            }
            template += '<p><a class="library" title="View included games"><span class="count"></span> games</a></p>';
            node.innerHTML = template;
            domClass.add(node, 'stat');
            domConstruct.place(node, container, 'last');

            var anchor = query('a.library', node)[0];
                anchor.href = library_path + '#';
            anchor.href += spectrum_into_filter_parameter(spectrum) + '=' + encodeURIComponent(stat.filter);

            query('>h3', node)[0].innerHTML = stat.label;
            query('span.count', node)[0].innerHTML = stat.count;
            if (metric == 'hp') { 
                query('strong.value', node)[0].innerHTML = region['format-hours'](stat.value);
            } else if (metric == 'dp') {
                if (region['symbol-position'] == 'before') {
                    query('strong.value', node)[0].innerHTML = 
                        region['symbol'] + region['format-dollars'](stat.value);
                } else {
                    query('strong.value', node)[0].innerHTML =
                        region['format-dollars'](stat.value) + region['symbol'];
                }
            }

            render_detail_chart(
                query('.chart', node)[0],
                (stat.value / sum) * 100,
                colors[0], colors[1],
                parents[j]
            );
        });
    };

    //--------------------------------------------------------------------------
    var render_detail_chart = function(container, percent, color_1, color_2, large_arc) {
        /**
         * Render a donut chart detailing a value breakdown.
         *
         * container: The container node for the chart.
         * percent: A value from [0, 100]
        **/

        var arc_tween = function(a) {
            var title_node = query('.title', container)[0];
            var i = d3.interpolate(this._current, a);
            this._current = i(0);

            return function(t) {
                var d = i(t);
                if (d.data.index == 0) {
                    title_node.innerHTML = d.value.toFixed(0) + '<span>%</span>';
                }
                return arc(d);
            };
        };

        var svg = d3.select(container).append('svg')
            .attr('width', 52)
            .attr('height', 52)
            .append('g')
            .attr('transform', 'translate(26, 26)');

        var color_scale_1 = d3.scale.ordinal().range([color_1, color_2]);
        var color_scale_2 = d3.scale.ordinal().range(['#000000', color_2]);
        var color_1 = function(d) { return color_scale_1(d.data.index); };
        var color_2 = function(d) { return color_scale_2(d.data.index); };
        var arc = d3.svg.arc()
            .outerRadius(25)
            .innerRadius(20);
        var pie = d3.layout.pie()
            .sort(null)
            .value(function(d) { return d.initial; });

        var path = svg.datum([
                {index: 0, value: percent, initial: 0},
                {index: 1, value: 100 - percent, initial: 100}
            ]).selectAll('path')
            .data(pie)
            .enter()
            .append('path')
            .attr('class', 'arc')
            .attr('d', arc)
            .attr('fill', color_1)
            .each(function(d) { this._current = d; });

        // Animate the donut chart into its final orientation.
        pie.value(function(d) { return d.value; });
        path = path.data(pie);
        path.transition().duration(750).ease('quad-in-out').attrTween('d', arc_tween);

        on(large_arc, mouse.enter, function() {
            // reset the chart into the initial position.
            pie.value(function(d) { return d.initial; });
            path = path.data(pie);
            path.attr('d', arc)
                .attr('fill', color_2)
                .each(function(d) { this._current = d; });
            
            // Animate the donut chart into its final orientation.
            pie.value(function(d) { return d.value; });
            path = path.data(pie);
            path.transition().duration(750).ease('quad-in-out').attrTween('d', arc_tween);
        });
        on(large_arc, mouse.leave, function() {
            path.attr('fill', color_1);
        });
    };

    //-------------------------------------------------------------------------- 
    var setup_friends = function() {
        /**
         * Setup the behaviour to invite friends to compare Insights.
        **/
        var modal = dom.byId('invitation-modal');
        if (modal) {
            var friends = dom.byId('friends');
            var message = dom.byId('message');
            var email_input = query('input.email', modal)[0];
            query(modal).modal({show: false});

            on(query('>a', friends), 'click', function(evt) {
                event.stop(evt);
                query(modal).modal('show');
            });

            var rpc = jsonrpc('/jsonrpc');
            on(query('form', modal), 'submit', function(evt) {
                event.stop(evt);

                var email = email_input.value;
                    email = email.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                if (!email) {
                    return;
                }

                rpc.request({
                    method: 'useraccountinvitation.create',
                    params: ['', {email: email}]

                }, function(data) {
                    email_input.value = '';
                    query(modal).modal('hide');

                    domClass.remove(message, 'hidden');
                    query('p', message)[0].innerHTML = 
                        'Your invitation has been sent! Once your friend accepts the invitation, you will be able to view their Library.'; 
                    fx.animateProperty({
                        node: dom.byId('message'),
                        properties: {
                            height: {start: 0, end: 44},
                            opacity: {start: 0, end: 1},
                            paddingBottom: {start: 0, end: 20}
                        }
                    }).play();

                }, function(error) {
                    query(email_input).tooltip('destroy');
                    query(email_input).tooltip({
                        trigger: 'manual',
                        placement: 'top',
                        html: false,
                        title: email + ' is not a valid e-mail address'
                    });
                    query(email_input).tooltip('show');
                });
            });

            on(email_input, 'focus', function(evt) {
                query(email_input).tooltip('destroy');
            });

            on(modal, 'hide', function(evt) {
                // Hide any error tooltips when the modal gets hidden.
                query(email_input).tooltip('destroy');
            });

            on(query('.dismiss', 'message'), 'click', function(evt) {
                event.stop(evt);
                 
                fx.animateProperty({
                    node: dom.byId('message'),
                    properties: {
                        opacity: {start: 1, end: 0},
                        height: {start: 44, end: 0},
                        paddingBottom: {start: 20, end: 0}
                    },
                    onEnd: function() {
                        domClass.add(dom.byId('message'), 'hidden');
                    }
                }).play();
            });
        }
    };

    //--------------------------------------------------------------------------
    var keys_for_spectrum = function(game, spectrum) {
        /**
         * For a requested spectrum, what values are assigned to a game?
         *
         * game: An object containing the game information.
         * spectrum: The spectrum to retrieve that game values for.
         *
         * Returns an array of objects that have two attributes;
         *     label - The human readable label for the games value.
         *     filter - The system filter for the games value (Used on the Library page)
        **/
        if (spectrum == 'Library/Enjoyment') {
            var label_map = {
                5: {label: '★★★★★', filter: '5'},
                4: {label: '★★★★', filter: '4'},
                3: {label: '★★★', filter: '3'}, 
                2: {label: '★★', filter: '2'},
                1: {label: '★', filter: '1'}, 
                0: {label: 'Unrated', filter: '0'}
            };
            return [label_map[game.r]];

        } else if (spectrum == 'Library/Finished') {
            if (game.f) {
                return [{label: 'Finished', filter: 'Yes'}];
            } else {
                return [{label: 'Unfinished', filter: 'No'}];
            }

        } else if (spectrum == 'Library/Hours') {
            if (game.hp < 1) {
                return [{label: '<1 hr', filter: '0-1'}];
            } else if (game.hp < 8) {
                return [{label: '1 - 8 hrs', filter: '1-8'}];
            } else if (game.hp < 20) {
                return [{label: '8 - 20 hrs', filter: '8-20'}];
            } else if (game.hp < 40) {
                return [{label: '20 - 40 hrs', filter: '20-40'}];
            } else {
                return [{label: '40+ hrs', filter: '40-100000'}];
            }

        } else if (spectrum == 'Library/Cost') {
            for (var i = 0; i < region['cost-filter-list'].length; ++i) {
                var bounds = region['cost-filter-list'][i]['filter'].split('-');
                bounds[0] = parseInt(bounds[0]);
                bounds[1] = parseInt(bounds[1]);

                if (game.dp >= bounds[0] && game.dp < bounds[1]) {
                    return [region['cost-filter-list'][i]];
                }
            }
        } else if (spectrum == 'Steam/Genre') {
            return game.g;

        } else {
            var system = game.t[spectrum];
            if (!system) {
                spectrum = spectrum.split('/')[1];
                return [{label: 'No Tag', filter: spectrum + ' / No Tag'}];
            }
            return system;
        }
    };

    //--------------------------------------------------------------------------
    var object_to_array = function(obj) {
        /**
         * Convert an object into a sorted array.
        **/
        var a = [];
        for (var k in obj) {
            if (obj[k].value > 0) {
                a.push(obj[k]);
            }
        }
        a.sort(function(a, b) { return b.value - a.value; });
        return a;
    };

    //----------------------------------------------------------------------
    var generate_analysis = function(data, metric, spectrum) {
        /**
         * Compile a summary for a set of data over a given spectrum.
         *
         * data - An array of game records.
         * metric - The attribute to summarize, 'hp', 'dp', etc.
         * spectrum - The spectrum to summarize
        **/

        var total = 0;
        var summary = {};
        var value = 0;

        var game, keys, key;
        for (var i = 0; i < data.length; ++i) {
            game = data[i];

            keys = keys_for_spectrum(game, spectrum);
            if (keys.length > 0) {
                value = game[metric] / keys.length;
            } else {
                value = 0;
            }

            for (var j = 0; j < keys.length; ++j) {
                key = keys[j];
                if (!summary[key.label]) {
                    summary[key.label] = {
                        value: value,
                        label: key.label,
                        filter: key.filter,
                        count: 1
                    };
                } else {
                    summary[key.label].value += value;
                    summary[key.label].count += 1;
                }
                total += value;
            }
        }

        return object_to_array(summary);
    };

    //--------------------------------------------------------------------------
    var init = function(data, library_path) {

        // Setup the interface to invite friends.
        setup_friends();

        //----------------------------------------------------------------------
        var multi_spectrum_analysis = function(library, metric, spectrums, colors) {
            /**
             * Produce a cross product summary of a game library over multiple spectrums.
             *
             * library: An array of game data.
             * metric: The value to summarize on the game data. Ex: 'hp', 'dp', etc.
             * spectrums: An array of spectrums to drill into.
             * colors: An array of [color1, color2] declarations. Must be the
             *         same length as the spectrums array.
             *
             * An object with these properties is returned:
             *     var s = {
             *         label: 'Total',
             *         value: 1234.23,
             *         colors: ['#ffffff', '#000000'],
             *         breakdown: <duplicate structure>
             *     };
            **/
            var summary = {
                value: d3.sum(library, function(d) { return d[metric]; }),
                label: '',
                filter: '',
                breakdown: {},
                colors: colors[0],
                source: []
            };

            var summary_into_array = function(summary) {
                /**
                 * Do some post-processing to the summary.
                **/

                // Sort the source array and only take the first 5 elements.
                summary.source.sort(function(a, b) {
                    return b.value - a.value;
                });
                summary.source = summary.source.slice(0, 2);

                // Recursively convert the breakdown properties.
                summary.breakdown = object_to_array(summary.breakdown);
                for (var i = 0; i < summary.breakdown.length; ++i) {
                    summary_into_array(summary.breakdown[i]);
                }
            };

            var highlight_summary = function(summary) {
                if (summary.breakdown.length) {
                    summary.breakdown[0].active = true;
                    highlight_summary(summary.breakdown[0]);
                }
            };

            var breakdown_value = function(
                breakdown,
                value,
                game,
                parent_label,
                parent_filter,
                spectrums,
                colors,
                depth
            ) {
                /**
                 * Apply an average value to the appropriate spectrum keys.
                **/
                var filter_parameter = spectrum_into_filter_parameter(spectrums[depth]) + '=';
                var keys = keys_for_spectrum(game, spectrums[depth]);
                if (keys.length < 1) {
                    return;
                }
                value = value / keys.length;

                var key, source, filter;
                for (var i = 0; i < keys.length; ++i) {
                    key = keys[i];
                    source = {id: game.id, name: game.n, value: value};

                    if (parent_filter) {
                        filter = parent_filter + '&' + filter_parameter + encodeURIComponent(key.filter);
                    } else {
                        filter = filter_parameter + encodeURIComponent(key.filter);
                    }

                    if (!breakdown[key.label]) {
                        breakdown[key.label] = {
                            value: value,
                            label: parent_label ? parent_label + ' ∩ ' + key.label : key.label,
                            filter: filter,
                            breakdown: {},
                            colors: colors[depth + 1],
                            source: [source],
                            count: 1
                        };
                    } else {
                        breakdown[key.label].value += value;
                        breakdown[key.label].source.push(source);
                        breakdown[key.label].count += 1;
                    }
                    if (depth < spectrums.length - 1) { 
                        breakdown_value(
                            breakdown[key.label].breakdown,
                            value,
                            game,
                            parent_label ? parent_label + ' ∩ ' + key.label : key.label,
                            filter,
                            spectrums,
                            colors,
                            depth + 1
                        );
                    }
                }
            };

            for (var i = 0; i < library.length; ++i) {
                var game = library[i];
                breakdown_value(
                    summary.breakdown,
                    game[metric],
                    game,
                    summary.label,
                    summary.filter,
                    spectrums,
                    colors,
                    0
                );
            }
            summary_into_array(summary);
            highlight_summary(summary);
            return summary;
        };

        var metric = 'hp';
        var spectrums = [
            dom.byId('by-blue').getAttribute('data-spectrum'),
            dom.byId('by-green').getAttribute('data-spectrum'),
            dom.byId('by-orange').getAttribute('data-spectrum')
        ]; 
        var colors = [
            ['#029bbd', '#a6e7f2'], ['#708f05', '#d8e89d'], ['#ff8c00', '#ffd89a']
        ];

        render_composite_chart(
            query('>.combined', 'by-blue')[0], 
            generate_analysis(data, metric, spectrums[0]),
            metric, spectrums[0], colors[0],
            library_path
        );
        render_composite_chart(
            query('>.combined', 'by-green')[0],
            generate_analysis(data, metric, spectrums[1]),
            metric, spectrums[1], colors[1],
            library_path
        );
        render_composite_chart(
            query('>.combined', 'by-orange')[0],
            generate_analysis(data, metric, spectrums[2]),
            metric, spectrums[2], colors[2],
            library_path
        );

        render_drilldown_chart(
            query('.chart', 'drilldown')[0],
            multi_spectrum_analysis(
                data, metric, spectrums, colors
            ),
            metric
        );
        add_drilldown_interaction(metric, library_path);

        // Toggle the display of all details in the drilldown chart.
        query('.toggle-display', 'drilldown')[0].checked = true;
        query('.toggle-display', 'drilldown').on('change', function(evt) {
            if (!evt.currentTarget.checked) {
                domClass.add(dom.byId('drilldown'), 'show');

                if (metric == 'hp') {
                    analytics.trackEvent('Time Insights', 'Drilldown', 'Show');
                } else {
                    analytics.trackEvent('Money Insights', 'Drilldown', 'Show');
                }
            } else {
                domClass.remove(dom.byId('drilldown'), 'show');

                if (metric == 'hp') {
                    analytics.trackEvent('Time Insights', 'Drilldown', 'Hide');
                } else {
                    analytics.trackEvent('Money Insights', 'Drilldown', 'Hide');
                }
            } 
        });

        // Give the dropdown menus some life.
        var update_column = function(column, label, spectrum) {
            /**
             * Update a column with a new spectrum.
            **/
            query('>h2 span', column.node)[0].innerHTML = label;
            render_composite_chart(
                query('>.combined', column.node)[0],
                generate_analysis(
                    data, metric,
                    spectrum
                ),
                metric, spectrum, [column.color_1, column.color_2],
                library_path
            );
            column.node.setAttribute('data-spectrum', spectrum);
        };

        var columns = [
            {node: dom.byId('by-blue'), color_1: '#029bbd', color_2: '#a6e7f2'}, 
            {node: dom.byId('by-green'), color_1: '#708f05', color_2: '#d8e89d'},
            {node: dom.byId('by-orange'), color_1: '#ff8c00', color_2: '#ffd89a'}
        ];
        array.forEach(columns, function(column, i) {
            query('.dropdown a', column.node).on('click', function(evt) {
                event.stop(evt);

                // Special case for the Premium Account nag link.
                if (domClass.contains(evt.currentTarget, 'premium-support')) {
                    return;
                }

                domClass.add(query('.dropdown >.options', column.node)[0], 'hidden');

                var drilldown_captions = query('>.caption >span', 'drilldown');
                var label = evt.currentTarget.textContent;
                var spectrum = evt.currentTarget.getAttribute('data-spectrum');
                var old_label = query('>h2 span', column.node)[0].textContent;
                var old_spectrum = column.node.getAttribute('data-spectrum');

                if (metric == 'hp') {
                    analytics.trackEvent('Time Insights', spectrum, 'column-' + (i + 1));
                } else {
                    analytics.trackEvent('Money Insights', spectrum, 'column-' + (i + 1));
                }

                // Swap two columns if they are going to show the same spectrum.
                array.some(columns, function(other, j) {
                    if (i == j) {
                        return;
                    }
                    if (other.node.getAttribute('data-spectrum') != spectrum) {
                        return;
                    }

                    update_column(other, old_label, old_spectrum); 
                    spectrums[j] = old_spectrum;
                    drilldown_captions[j].innerHTML = old_label; 
                    return true;
                });

                update_column(column, label, spectrum);
                drilldown_captions[i].innerHTML = label; 
                spectrums[i] = spectrum;
                render_drilldown_chart(
                    query('.chart', 'drilldown')[0],
                    multi_spectrum_analysis(
                        data, metric, spectrums, colors
                    ),
                    metric
                );
                add_drilldown_interaction(metric, library_path);

                // Re-enable the drop down after a short amount of time.
                window.setTimeout(function() {
                    domClass.remove(query('.dropdown >.options', column.node)[0], 'hidden'); 
                }, 125);
            });
        });

        // Toggle between Time & Money.
        var tabs = query('.tab', 'tab-list');
        on(tabs, 'click', function(evt) {
            event.stop(evt);
            
            var tab = evt.currentTarget;
            tabs.forEach(function(tab) {
                domClass.remove(tab, 'active');
            }); 

            domClass.add(tab, 'active');
            metric = tab.getAttribute('data-metric');

            if (metric == 'hp') {
                analytics.trackEvent('Time Insights', 'Tab');
            } else {
                analytics.trackEvent('Money Insights', 'Tab');
            }

            if (metric == 'hp') {
                query('>h2', 'drilldown')[0].innerHTML = 
                    'Where do I spend my time?';
            } else {
                query('>h2', 'drilldown')[0].innerHTML =
                    'Where do I spend my money?';
            }

            // Re-render the entire insights.
            render_composite_chart(
                query('>.combined', 'by-blue')[0],
                generate_analysis(data, metric, spectrums[0]),
                metric, spectrums[0], colors[0],
                library_path
            );
            render_composite_chart(
                query('>.combined', 'by-green')[0],
                generate_analysis(data, metric, spectrums[1]),
                metric, spectrums[1], colors[1],
                library_path
            );
            render_composite_chart(
                query('>.combined', 'by-orange')[0],
                generate_analysis(data, metric, spectrums[2]),
                metric, spectrums[2], colors[2],
                library_path
            );
            render_drilldown_chart(
                query('>.chart', 'drilldown')[0],
                multi_spectrum_analysis(
                    data, metric, spectrums, colors
                ),
                metric
            );
            add_drilldown_interaction(metric, library_path);
        });
    };

    return init;
});
